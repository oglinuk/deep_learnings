# Convolutional Neural Network

A CNN written in Python using the Keras and Tensorflow libraries to recognize digits from the MNIST set.

## Output
```
60000/60000 [==============================] - 7s 119us/step - loss: 0.2472 - acc: 0.9243 - val_loss: 0.1297 - val_acc: 0.9584
Epoch 2/7
60000/60000 [==============================] - 8s 137us/step - loss: 0.1015 - acc: 0.9691 - val_loss: 0.0785 - val_acc: 0.9752
Epoch 3/7
60000/60000 [==============================] - 8s 128us/step - loss: 0.0748 - acc: 0.9776 - val_loss: 0.0765 - val_acc: 0.9782
Epoch 4/7
60000/60000 [==============================] - 9s 142us/step - loss: 0.0597 - acc: 0.9820 - val_loss: 0.0749 - val_acc: 0.9811
Epoch 5/7
60000/60000 [==============================] - 8s 140us/step - loss: 0.0508 - acc: 0.9845 - val_loss: 0.0750 - val_acc: 0.9811
Epoch 6/7
60000/60000 [==============================] - 8s 133us/step - loss: 0.0437 - acc: 0.9870 - val_loss: 0.0812 - val_acc: 0.9792
Epoch 7/7
60000/60000 [==============================] - 7s 116us/step - loss: 0.0397 - acc: 0.9879 - val_loss: 0.0781 - val_acc: 0.9814
Test score:  0.07808684695483553
Test accuracy:  0.9814
```