from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.utils.np_utils import to_categorical

class MNIST_CNN():
    def __init__(self):
        (self.x_train, self.y_train), (self.x_test, self.y_test) = self.load_mnist_data()
        self.model = self.build_model()
        self.model.compile(optimizer='rmsprop',
                            loss='categorical_crossentropy',
                            metrics=['accuracy'])

    def load_mnist_data(self):
        (x_train, y_train), (x_test, y_test) = mnist.load_data()

        x_train = x_train.reshape(60000, 784)
        x_test = x_test.reshape(10000, 784)
        x_train = x_train.astype('float32')
        x_test = x_test.astype('float32')
        x_train /= 255
        x_test /= 255

        y_train = to_categorical(y_train, 10)
        y_test = to_categorical(y_test, 10)

        return (x_train, y_train), (x_test, y_test)

    def build_model(self):
        model = Sequential()
        model.add(Dense(512, input_shape=(784,)))
        model.add(Activation('relu'))

        model.add(Dropout(rate=0.2))
        model.add(Dense(512))
        model.add(Activation('relu'))
        model.add(Dropout(rate=0.2))
        model.add(Dense(10))
        model.add(Activation('softmax'))

        return model

    def train_model(self):
        self.model.fit(self.x_train, self.y_train, batch_size=128, epochs=7,
                        verbose=1, validation_data=(self.x_test, self.y_test))

    def score_model(self):
        score = self.model.evaluate(self.x_test, self.y_test, verbose=0)
        #batch_size=32, verbose=1, sample_weight=None)
        print('Test score: ', score[0])
        print('Test accuracy: ', score[1])

if __name__ == '__main__':
    mnist_cnn = MNIST_CNN()
    mnist_cnn.train_model()
    mnist_cnn.score_model()